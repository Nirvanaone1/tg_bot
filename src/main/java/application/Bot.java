package application;

import org.telegram.telegrambots.extensions.bots.commandbot.TelegramLongPollingCommandBot;
import service.HelpCommand;
import org.telegram.telegrambots.meta.api.methods.send.SendMessage;
import org.telegram.telegrambots.meta.api.objects.Message;
import org.telegram.telegrambots.meta.api.objects.Update;
import org.telegram.telegrambots.meta.exceptions.TelegramApiException;
import service.PictureCommand;
import service.StartCommand;
import service.RandomNumberCommand;
public class Bot extends TelegramLongPollingCommandBot {
    private final String BOT_TOKEN;
    private final String BOT_USERNAME;
    public Bot(String botToken, String bot_username) {
        super();
        BOT_TOKEN = botToken;
        BOT_USERNAME = bot_username;
        register(new RandomNumberCommand("random", "Рандом"));
        register(new StartCommand("start", "старт"));
        register(new HelpCommand("help", "помощь"));
        register(new PictureCommand("picture", "получить картинку"));
    }
    @Override
    public String getBotUsername() {
        return this.BOT_USERNAME;
    }

    @Override
    public String getBotToken() {
        return this.BOT_TOKEN;
    }

    // Ответ на запрос, не являющийся командой
    @Override
    public void processNonCommandUpdate(Update update) {
        Message message = update.getMessage();
        sendAnswer(message.getChatId(), getUserName(message));
    }
    public String getUserName(Message msg){
        return msg.getFrom().getUserName();
    }

    private void sendAnswer(Long chatId, String userName){
        SendMessage answer = new SendMessage();
        answer.setChatId(chatId.toString());
        answer.setText("Непонятная команда, обратитесь к команде /help");
        try {
            execute(answer);
        } catch (TelegramApiException e) {
            System.out.println("Ошибка отправки сообщения от - " + userName);
        }
    }
}
