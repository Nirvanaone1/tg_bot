package service;

import org.telegram.telegrambots.meta.api.objects.Chat;
import org.telegram.telegrambots.meta.api.objects.User;
import org.telegram.telegrambots.meta.bots.AbsSender;

public class HelpCommand extends BaseCommandService {
    public HelpCommand(String commandIdentifier, String description) { super(commandIdentifier, description);}

    @Override
    public void execute(AbsSender absSender, User user, Chat chat, String[] strings){
        sendAnswer(absSender, chat.getId(), this.getCommandIdentifier(), user.getUserName(), "Я тренировочный бот. Список доступных функций: /random");
    }
}
