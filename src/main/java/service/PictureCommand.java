package service;

import org.telegram.telegrambots.meta.api.methods.send.SendPhoto;
import org.telegram.telegrambots.meta.api.objects.Chat;
import org.telegram.telegrambots.meta.api.objects.InputFile;
import org.telegram.telegrambots.meta.api.objects.User;
import org.telegram.telegrambots.meta.bots.AbsSender;
import org.telegram.telegrambots.meta.exceptions.TelegramApiException;
import java.net.MalformedURLException;
import java.net.URL;

public class PictureCommand extends BaseCommandService{
    public PictureCommand(String commandIdentifier, String description) {
        super(commandIdentifier, description);
    }

    @Override
    public void execute(AbsSender absSender, User user, Chat chat, String[] strings){
        String picUrl = "https://img10.joyreactor.cc/pics/post/%D0%AD%D1%80%D0%BE%D1%82%D0%B8%D0%BA%D0%B0-%D0%BC%D0%BE%D0%BB%D0%BE%D1%87%D0%BD%D1%8B%D0%B5-%D1%82%D1%8F%D0%BD-%D0%90%D0%B7%D0%B8%D0%B0%D1%82%D0%BA%D0%B0-COVID-19-7660358.jpeg";
        try{
            absSender.execute(getPhoto(chat.getId(), picUrl));
        } catch (TelegramApiException e) {
            throw new RuntimeException(e);
        } catch (MalformedURLException e) {
            throw new RuntimeException(e);
        }
    }

    private SendPhoto getPhoto(Long chatId, String ImgUrl) throws MalformedURLException {
        URL url = new URL(ImgUrl);
        InputFile photo = new InputFile(String.valueOf(url));
        SendPhoto sPhoto = new SendPhoto();
        sPhoto.setPhoto(photo);
        sPhoto.setChatId(chatId.toString());
        sPhoto.setCaption("asian girl");
        return sPhoto;
    }
}
