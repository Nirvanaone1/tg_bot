package service;

import org.telegram.telegrambots.extensions.bots.commandbot.commands.BotCommand;
import org.telegram.telegrambots.meta.api.methods.send.SendMessage;
import org.telegram.telegrambots.meta.api.objects.Chat;
import org.telegram.telegrambots.meta.api.objects.User;
import org.telegram.telegrambots.meta.bots.AbsSender;
import org.telegram.telegrambots.meta.exceptions.TelegramApiException;

public class BaseCommandService extends BotCommand {

    BaseCommandService(String commandIdentifier, String description){
        super(commandIdentifier, description);
    }

    @Override
    public void execute(AbsSender absSender, User user, Chat chat, String[] strings) {

    }
    void sendAnswer(AbsSender absSender, Long chatId, String commandName, String userName, String answer){
        SendMessage sendMessage = new SendMessage();
        sendMessage.setChatId(chatId.toString());
        sendMessage.enableMarkdown(true);
        sendMessage.setText(answer);

        try{
            absSender.execute(sendMessage);
        } catch (TelegramApiException e) {
            System.out.println("Ошибка в базовом классе CommandService с чат id - " + chatId + "\n");
            e.printStackTrace();
        }
    }

}
