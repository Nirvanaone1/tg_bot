package service;

import lombok.SneakyThrows;
import org.telegram.telegrambots.meta.api.methods.send.SendDocument;
import org.telegram.telegrambots.meta.api.objects.Chat;
import org.telegram.telegrambots.meta.api.objects.InputFile;
import org.telegram.telegrambots.meta.api.objects.User;
import org.telegram.telegrambots.meta.bots.AbsSender;
import org.telegram.telegrambots.meta.exceptions.TelegramApiException;
import utils.WordFileProcessImpl;
import java.io.FileInputStream;
import java.io.IOException;

public class RandomNumberCommand extends BaseCommandService {

    public RandomNumberCommand(String commandIdentifier, String description) {
        super(commandIdentifier, description);
    }
    @SneakyThrows
    @Override
    public void execute(AbsSender absSender, User user, Chat chat, String[] strings){
        try{
            absSender.execute(createDocument(chat.getId()));
        } catch ( TelegramApiException e) {
            e.printStackTrace();
        }
    }

    private  SendDocument createDocument(Long chatId) throws IOException {
        WordFileProcessImpl fileGeneration = new WordFileProcessImpl();
        FileInputStream fileInputStream = fileGeneration.createTempFile();
        SendDocument document = new SendDocument();
        document.setChatId(chatId.toString());
        document.setDocument(new InputFile(fileInputStream, "randomNumbers.docx"));
        return document;
    }


}
